using Backend.Models;
using System;
using Xunit;

namespace Backend.testes
{
    public class ProdutosTeste
    {
        [Fact]
        public void TestarValor()
        {
            Products prd = new Products();
            var result = prd.CalcularValorTotal(3, 3);
            Assert.Equal(9, result);
        }
    }
}
