using Backend.Models;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Backend.testes
{
    public class InvoicesTeste
    {
        [Fact]
        public static void TesteValorTotalFatura()
        {

            var invoice = (new Invoices
            {
                Referencia = "Ref1",
                estado = "activa",
                ListaProduct = new List<Products> {
                new Products { Id = 1, Ref = "r0001", Quantidade = 30, Descricao = "Comida para cão", Preco = 10, taxa = new Taxas { idTaxa = 1, percentagemTaxa = 0.23 } },
                new Products{  Id = 2, Ref = "r0002", Quantidade = 40, Descricao = "Batatas", Preco = 1, taxa = new Taxas { idTaxa = 2, percentagemTaxa = 0.06 } } }
            });

            var total = InvoicesDomain.invoiceValorTotal(invoice);

            Assert.Equal(340, total);

        }
    }
}