using System;
using System.Collections.Generic;

namespace Backend.Models
{
    public static class InvoicesDomain
    {
        public static List<Invoices> listaInvoices(List<Invoices> invoice)
        {
            foreach (Invoices inv in invoice)
            {

                foreach (Products prod in inv.ListaProduct)
                {
                    prod.valorTotal = prod.CalcularValorTotal(prod.Preco, prod.Quantidade);
                    prod.valorIva = prod.calcularValorIva(prod.valorTotal, prod.taxa.percentagemTaxa);
                    inv.valorIvaTotal += prod.valorIva;
                    inv.PrecoTotal += prod.valorTotal;
                }
            }
            return invoice;
        }
        public static double invoiceValorTotal(Invoices inv)
        {


            foreach (Products prod in inv.ListaProduct)
            {
                prod.valorTotal = prod.CalcularValorTotal(prod.Preco, prod.Quantidade);
                prod.valorIva = prod.calcularValorIva(prod.valorTotal, prod.taxa.percentagemTaxa);
                inv.valorIvaTotal += prod.valorIva;
                inv.PrecoTotal += prod.valorTotal;
            }

            return inv.PrecoTotal;
        }

    }
}