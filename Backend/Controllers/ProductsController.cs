﻿using System;
using System.Collections.Generic;
using System.Linq;
using Backend.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Backend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductsController : ControllerBase
    {
        BackendContext _context = new BackendContext();

        private readonly List<Products> Products = new List<Products>();

        private readonly ILogger<ProductsController> _logger;

        public ProductsController(ILogger<ProductsController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<Products> Get()
        {
            var products = _context.Product.ToList();
            return products;
        }

        [HttpGet("{id}")]
        public ActionResult<Products> Get(int id)
        {
            //var product = Products.FirstOrDefault((p) => p.Id == id);
            var product = _context.Product.Find(id);

            if (product == null)
            {
                return NotFound();
            }

            return Ok(product);
        }

        [HttpPost]
        public ActionResult Create(Products product)
        {
            try
            {
                _context.Product.Add(product);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                BadRequest(ex.Message);
            }

            return Ok(product);

        }

        [HttpPut("{id}")]
        public ActionResult Update(int id, Products product)
        {
            if (id != product.Id)
            {
                return BadRequest();
            }

            //Products manterAlteracao = Products.FirstOrDefault(x => x.Id == id);
            _context.Entry(product).State = EntityState.Modified;

            try
            {               
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        private bool ProductsExists(int id)
        {
            throw new NotImplementedException();
        }
    }
}