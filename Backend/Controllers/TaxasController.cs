using System;
using System.Collections.Generic;
using System.Linq;
using Backend.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Backend.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class TaxasController : ControllerBase
    {

        BackendContext _context = new BackendContext();

        private readonly ILogger<TaxasController> _logger;

        public TaxasController(ILogger<TaxasController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<Taxas> Get()
        {
            var taxas = _context.Taxa.ToList();
            return taxas;
        }

        [HttpGet("{idTaxa}")]
        public ActionResult<Taxas> Get(int idTaxa)
        {
            
            var taxas = _context.Taxa.Find(idTaxa);

            if (taxas == null)
            {
                return NotFound();
            }

            return Ok(taxas);
        }

        [HttpPost]
        public ActionResult Create(Taxas taxa)
        {
            try
            {
                _context.Taxa.Add(taxa);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                BadRequest(ex.Message);
            }

            return Ok(taxa);

        }

        [HttpPut("{idTaxa}")]
        public ActionResult Update(int idTaxa, Taxas taxa)
        {
            if (idTaxa != taxa.idTaxa)
            {
                return BadRequest();
            }

            _context.Entry(taxa).State = EntityState.Modified;

            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TaxasExists(idTaxa))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        private bool TaxasExists(int idTaxa)
        {
            throw new NotImplementedException();
        }

    }
}