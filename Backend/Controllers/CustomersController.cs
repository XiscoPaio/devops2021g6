﻿using System;
using System.Collections.Generic;
using System.Linq;
using Backend.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Backend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CustomersController : ControllerBase
    {
        BackendContext _context = new BackendContext();

        private readonly ILogger<CustomersController> _logger;

        public CustomersController(ILogger<CustomersController> logger)
        {
            _logger = logger;

        }


        [HttpGet]
        public IEnumerable<Customers> Get()
        {
            var customers = _context.Customer.ToList();
            return customers;
        }

        [HttpGet("{id}")]
        public ActionResult<Customers> Get(int id)
        {
            //var customer = Customers.FirstOrDefault((p) => p.Id == id);
            var customer = _context.Customer.Find(id);

            if (customer == null)
            {
                return NotFound();
            }

            return Ok(customer);
        }

        [HttpPost]
        public ActionResult Create(Customers customer)
        {          

            try
            {
                customer.Status = true;
                customer.RegisterDate = DateTime.UtcNow;
                
                _context.Customer.Add(customer);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                BadRequest(ex.Message);
            }

            return Ok(customer);

        }

        [HttpPut("{id}")]
        public ActionResult Update(int id, Customers customer)
        {
            if (id != customer.Id)
            {
                return BadRequest();
            }

            _context.Entry(customer).State = EntityState.Modified;

            try
            {
                customer.Status = true;
                customer.RegisterDate = DateTime.UtcNow;
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CustomersExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
            //Customers todoCustomer = Customers.FirstOrDefault(x=> x.Id == id);

            //if (todoCustomer == null)
            //{
                //return NotFound();
            //}

            //todoCustomer.Name = customer.Name;
            //todoCustomer.Status = customer.Status;

            //return Ok(todoCustomer);
        }

        private bool CustomersExists(int id)
        {
            throw new NotImplementedException();
        }
    }
}
