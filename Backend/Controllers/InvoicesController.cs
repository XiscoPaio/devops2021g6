using System;
using System.Collections.Generic;
using System.Linq;
using Backend.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Backend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class InvoicesController : ControllerBase
    {
        BackendContext _context = new BackendContext();

        private readonly List<Invoices> Invoices = new List<Invoices>();


        private readonly ILogger<InvoicesController> _logger;

        public InvoicesController(ILogger<InvoicesController> logger)
        {
            _logger = logger;

        }

        [HttpGet]
        public IEnumerable<Invoices> Get()
        {
            var invoices = _context.Invoice.ToList();
            return invoices;
        }

        [HttpGet("{referencia}")]
        public ActionResult<Invoices> Get(string referencia)
        {
            //var invoice = Invoices.FirstOrDefault((p) => p.Referencia == referencia);
            var invoice = _context.Invoice.Find(referencia);

            foreach (Products prod in invoice.ListaProduct)
            {

                prod.valorTotal = prod.CalcularValorTotal(prod.Preco, prod.Quantidade);
                prod.valorIva = prod.calcularValorIva(prod.valorTotal, prod.taxa.percentagemTaxa);
                invoice.valorIvaTotal += prod.valorIva;
            }

            if (invoice == null)
            {
                return NotFound();
            }

            return Ok(invoice);
        }

        [HttpPost]
        public ActionResult Create(Invoices invoice)
        {
            try
            {
                invoice.PrecoTotal = 0;

                _context.Invoice.Add(invoice);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                BadRequest(ex.Message);
            }

            return Ok(invoice);

        }

        [HttpPut("{referencia}")]
        public ActionResult Update(string referencia, Invoices invoice)
        {
            if (referencia != invoice.Referencia)
            {
                return BadRequest();
            }

            _context.Entry(invoice).State = EntityState.Modified;

            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!InvoicesExists(referencia))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        private bool InvoicesExists(string referencia)
        {
            throw new NotImplementedException();
        }
    }
}