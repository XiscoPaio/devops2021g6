﻿using System;
using Backend.Models;
using Microsoft.EntityFrameworkCore;

namespace Backend.Models
{
    class BackendContext : DbContext 
    {
        public BackendContext()
        {
            
        }

        public DbSet<Customers> Customer { get; set; }
        public DbSet<Products> Product { get; set; }
        public DbSet<Invoices> Invoice { get; set; }
        public DbSet<Taxas> Taxa { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=WIN-PQ79IDNVVKS;Database=mydb;User Id=sa;Password=Vagrant42;");
        }
    }
}
