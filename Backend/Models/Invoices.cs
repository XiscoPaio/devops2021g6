using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Backend.Models
{
    public class Invoices
    {
        [Key]
        public string Referencia { get; set; }
        public double PrecoTotal { get; set; }
        public double valorIvaTotal { get; set; }
        public string estado { get; set; }
        public ICollection<Products> ListaProduct { get; set; }
    }

}