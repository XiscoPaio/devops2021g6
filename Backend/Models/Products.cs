﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Backend.Models
{
    public class Products
    {
        [Key]
        public int Id { get; set; }
        public string Ref{ get; set; }
        public int Quantidade { get; set; }
        public string Descricao { get; set; }
        public double Preco { get; set;  }
        public double valorIva { get  ; set; }
        public Taxas taxa { get; set; }
        public double valorTotal { get; set; }
        public double CalcularValorTotal (double Preco, int Quantidade){
            return Preco * Quantidade;
        }
        public double calcularValorIva (double valorTotal, double taxa) {
            return valorTotal * taxa;
        }
        
    }
}