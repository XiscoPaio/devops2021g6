using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Backend.Models
{
    public class Taxas 
    {   
        [Key]
        public int idTaxa { get; set; }
        public double percentagemTaxa { get; set; }

    }
}